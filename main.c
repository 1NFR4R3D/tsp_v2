#include<stdio.h>
#include<stdlib.h>

#define size 4

int isProcessed[size], n=size, cost=0, costTable[size][size]={{0,4,1,3},{4,0,2,1},{1,2,0,5},{3,1,5,0}};

void printInput(){
	printf("\nCost Matrix - ");
	for(int i=0;i<size;i++){
		printf("\n");
		for(int j=0;j<size;j++){
			printf("\t%d",costTable[i][j]);
		}
	}
}

int nearCity(int c){
	int nc=size+1;
	int min=size+1,kmin;
	for(int i=0;i<size;i++){
		if((costTable[c][i]!=0)&&(isProcessed[i]==0))
			if(costTable[c][i]+costTable[i][c]<min){
				min=costTable[i][0]+costTable[c][i];
				kmin=costTable[c][i];
				nc=i;
			}
	}
 
	if(min!=size+1)
		cost+=kmin;
 
	return nc;
}
 
void minCost(int city)
{
	int i,nextCity;
 
	isProcessed[city]=1;
 
	printf("%d--->",city+1);
	nextCity=nearCity(city);
 
	if(nextCity==size+1)
	{
		nextCity=0;
		printf("%d",nextCity+1);
		cost+=costTable[city][nextCity];
 
		return;
	}
 
	minCost(nextCity);
}
 
int main()
{
	printInput();
 
	printf("\nPath -\n");
	minCost(0); //passing 0 because starting vertex
 
	printf("\n\nMinimum Cost = %d\n ",cost);
 
	return 0;
}